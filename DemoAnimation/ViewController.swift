//
//  ViewController.swift
//  DemoAnimation
//
//  Created by Macintosh on 7/1/19.
//  Copyright © 2019 Macintosh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var heightNavigation: NSLayoutConstraint!
    var isHeight = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func touchButton(_ sender: Any) {
        self.view.layoutIfNeeded()
        if isHeight {
            UIView.animate(withDuration: 0.5, animations: {
                self.heightNavigation.constant = 64
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.heightNavigation.constant = 108
                self.view.layoutIfNeeded()
            })
        }
        self.isHeight = !self.isHeight
    }
}

