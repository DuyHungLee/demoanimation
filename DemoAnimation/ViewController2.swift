//
//  ViewController2.swift
//  DemoAnimation
//
//  Created by Macintosh on 7/1/19.
//  Copyright © 2019 Macintosh. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    let shapeLayer = CAShapeLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let center = self.view.center
        let trackLayer = CAShapeLayer()
        let benzier = UIBezierPath(arcCenter: center, radius: 100, startAngle: -.pi/2, endAngle: 2 * .pi, clockwise: true)
        trackLayer.path = benzier.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth  = 10
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        
        view.layer.addSublayer(trackLayer)
       
        shapeLayer.path = benzier.cgPath
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth  = 10
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.strokeEnd = 0
        self.view.layer.addSublayer(shapeLayer)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchView)))
    }
    
    @objc func touchView() {
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
        basicAnimation.fillMode = .forwards
        basicAnimation.isRemovedOnCompletion = true
        basicAnimation.repeatCount = 10
        shapeLayer.add(basicAnimation, forKey: "s")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
